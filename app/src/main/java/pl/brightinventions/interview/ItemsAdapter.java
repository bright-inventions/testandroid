package pl.brightinventions.interview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemViewHolder> {
    private final List<String> items;

    ItemsAdapter(List<String> items) {
        this.items = items;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addItem(String name) {
        items.add(name);
    }

    public void removeItem(String name) {
        int position = items.indexOf(name);
        if (position != -1) {
            items.remove(position);
        }
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        final TextView name;

        ItemViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
        }
    }
}
